package com.myth.pictoria;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.util.LruCache;

import com.myth.pictoria.RequestDetail.ManagementType;

public class AssetHandleManager {

	private LruCache<String, Bitmap> loadedFiles;
	private String ASSET_LIST_NAME = "bitmapfilelist";
	private List<String> fileList;

	public AssetHandleManager(int cacheSize) {
		loadedFiles  = new LruCache<String, Bitmap>(cacheSize);
		fileList = new ArrayList<String>();
		this.initilize();
	}
	
	private void initilize() {
		this.deserializeList();
	}
	
	private boolean isFileExist(String filePath) {
		return fileList != null && fileList.contains(filePath);
	}

	class SerealizeThread extends Thread {
		@Override
		public void run() {
			try{
				File file = new File(AssetHandleManager.this.getFilePath()+"/"+ASSET_LIST_NAME);
				if (!file.exists()) {
					File parent = file.getParentFile();
					if (!parent.exists()) {
						parent.mkdirs();
					}
					if (!file.exists()) {
						file.createNewFile();
					}
				}
				FileOutputStream fos= new FileOutputStream(AssetHandleManager.this.getFilePath()+"/"+ASSET_LIST_NAME);
		        ObjectOutputStream oos= new ObjectOutputStream(fos);
		        oos.writeObject(fileList);
		        oos.close();
		        fos.close();
		    }catch(IOException ioe){
		        ioe.printStackTrace();
		    }
		}
	}

	
	private void deserializeList() {
		try
        {
			File file = new File(this.getFilePath()+"/"+ASSET_LIST_NAME);
			if (file.exists()) {
	            FileInputStream fis = new FileInputStream(this.getFilePath()+"/"+ASSET_LIST_NAME);
	            ObjectInputStream ois = new ObjectInputStream(fis);
	            fileList = (ArrayList<String>) ois.readObject();
	            ois.close();
	            fis.close();
			}
        }catch(IOException ioe){
            ioe.printStackTrace();
            return;
        }catch(ClassNotFoundException c){
            System.out.println("Class not found");
            c.printStackTrace();
            return;
        }
	}

	public void fetchAsset(RequestHandler requestHandler) {
		if (this.isFileExist(requestHandler.requestDetail.getFilePath())) {
			Bitmap bitmap = BitmapFactory.decodeFile(requestHandler.requestDetail.getFilePath());
			this.addToCache(requestHandler.requestDetail, bitmap);
			requestHandler.onComplete();
		} else {
			Runnable downloader;
			if (requestHandler.requestDetail.getImageUrl().contains("content:")) {
				downloader = new ContactDownloader(requestHandler);
			} else {
				downloader = new WebDownloader(requestHandler);
			}
			DownloadExecutor.submit(downloader, requestHandler);
		}
	}
	
	public void addToCache(RequestDetail requestDetail, Bitmap bitmap) {
		loadedFiles.put(requestDetail.getFilePath(), bitmap);
		if (ManagementType.DISK.equals(requestDetail.getManagementType())) {
			if (!fileList.contains(requestDetail.getFilePath())) {
				fileList.add(requestDetail.getFilePath());
				new SerealizeThread().start();
			}
		}
	}
	
	public Bitmap getBitmap(RequestDetail requestDetail) {
		Bitmap bitmap = loadedFiles.get(requestDetail.getFilePath());
		if (requestDetail.getHeight() != 0 && requestDetail.getWidth() != 0) {
			bitmap = Bitmap.createScaledBitmap(bitmap, requestDetail.getWidth(), requestDetail.getHeight(), false);
		}
		return bitmap;
	}
	
	public String getFilePath() {
		File directory = null;
        if (Environment.getExternalStorageState() == null) {
        	directory = new File(Environment.getDataDirectory()
                    + "/Pictoria/");
            if (!directory.exists()) {
                directory.mkdir();
            }
        } else if (Environment.getExternalStorageState() != null) {
        	directory = new File(Environment.getExternalStorageDirectory()
                    + "/Pictoria/");
            if (!directory.exists()) {
                directory.mkdir();
            }
        }
        return directory.getAbsolutePath();
	}
}