package com.myth.pictoria;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class AssetFileHandler implements IAssetHandler {

	byte[] dataArr;
	RequestDetail requestDetail;
	
	public AssetFileHandler(RequestDetail requestDetail) {
		dataArr = new byte[4096];
		this.requestDetail = requestDetail;
	}
	
	@Override
	public void addAsset(InputStream input) throws IOException {
		OutputStream output = null;
		File outputFile = new File(requestDetail.getFilePath());
		File parentFile = outputFile.getParentFile();
		if (parentFile != null) {
			parentFile.mkdirs();
		}
		if ( (parentFile.exists()) && (parentFile.isDirectory()) ) {
			if (outputFile.isDirectory()) {
				outputFile.delete();
			}
			if ( (!outputFile.exists()) || (!outputFile.isFile()) ) {
				outputFile.createNewFile();
			}
		}
        output = new FileOutputStream(requestDetail.getFilePath());
        int count = 0;
        while ((count = input.read(dataArr)) != -1) {
        	output.write(dataArr, 0, count);
        }
        output.flush();
        output.close();
        
        Bitmap bitmap = BitmapFactory.decodeStream(input);
        Pictoria.getInstance().getAssetHandleManager().addToCache(requestDetail, bitmap);
	}

}
