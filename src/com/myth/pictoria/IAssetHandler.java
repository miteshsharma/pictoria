package com.myth.pictoria;

import java.io.IOException;
import java.io.InputStream;

public interface IAssetHandler {

	public void addAsset(InputStream input) throws IOException;
	
}
