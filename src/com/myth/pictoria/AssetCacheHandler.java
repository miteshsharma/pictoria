package com.myth.pictoria;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class AssetCacheHandler implements IAssetHandler {

	RequestDetail requestDetail;
	
	public AssetCacheHandler(RequestDetail requestDetail) {
		this.requestDetail = requestDetail;
	}
	
	@Override
	public void addAsset(InputStream input) {
		Bitmap bitmap = BitmapFactory.decodeStream(input);
        Pictoria.getInstance().getAssetHandleManager().addToCache(requestDetail, bitmap);
	}

}
