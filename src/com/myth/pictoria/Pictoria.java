package com.myth.pictoria;

import android.content.Context;

public class Pictoria {
	
	public Context context;
	private static Pictoria mInstance;
	private AssetHandleManager assetHandleManager;
	private MemoryType memoryType;

	private Pictoria() {
		this.memoryType = MemoryType.SAVE_MEMORY;
	}

	public static synchronized void init(Context context) {
		Pictoria.getInstance().context = context;
	}
	
	public static synchronized void init(Context context, MemoryType memoryType) {
		Pictoria.getInstance().context = context;
		if (memoryType == null) {
			throw new IllegalArgumentException("Memory type can not be null.");
		}
		Pictoria.getInstance().memoryType = memoryType;
	}

	public static synchronized Pictoria getInstance() {
		if (mInstance == null) {
			mInstance = new Pictoria();
		}
		return mInstance;
	}
	
	public void addRequest(RequestDetail requestDetail) {
		if (requestDetail == null) {
			throw new IllegalArgumentException("Request detail can not be null.");
		}
		if (requestDetail.getImageUrl() == null || requestDetail.getDefaultResource() == null) {
			throw new IllegalArgumentException("Image url or default resource can not be null.");
		}
		RequestHandler requestHandler = new RequestHandler(requestDetail);
		requestHandler.start();
	}
	
	public static void dispose() {
		mInstance.assetHandleManager = null;
		mInstance = null;
	}

	public AssetHandleManager getAssetHandleManager() {
		if (assetHandleManager == null) {
			assetHandleManager = new AssetHandleManager(memoryType.getCacheSize());
		}
		return assetHandleManager;
	}
	
	public enum MemoryType {
		SAVE_MEMORY(10),
		SPEEDUP_PROCESSING(20),
		FAST_PROCESSING(50),
		IMAGE_EXTENSIVE(100);
		
		int cacheSize;
		
		MemoryType(int cacheSize) {
			this.cacheSize = cacheSize;
		}
		
		public int getCacheSize() {
			return this.cacheSize;
		}
	}
}
