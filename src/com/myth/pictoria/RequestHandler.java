package com.myth.pictoria;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;

public class RequestHandler implements ICallback {

	RequestDetail requestDetail;

	public RequestHandler(RequestDetail requestDetail) {
		this.requestDetail = requestDetail;
	}
	
	public void start() {
		this.requestDetail.imageView.setImageDrawable(this.requestDetail.getDefaultResource());
		Bitmap file = Pictoria.getInstance().getAssetHandleManager().getBitmap(this.requestDetail);
		if (file == null) {
			Pictoria.getInstance().getAssetHandleManager().fetchAsset(this);
		} else {
			this.onComplete();
		}
	}
	
	@Override
	public void onComplete() {
		final Bitmap file = Pictoria.getInstance().getAssetHandleManager().getBitmap(this.requestDetail);
		new Handler(Looper.getMainLooper()).post(new Runnable() {
		    @Override
		    public void run() {
				if (file != null) {
					Drawable d = new BitmapDrawable(Pictoria.getInstance().context.getResources(), file);
					RequestHandler.this.requestDetail.imageView.setImageDrawable(d);
				} else {
					RequestHandler.this.requestDetail.imageView.setBackgroundResource(RequestHandler.this.requestDetail.errorResourceId);
				}
		    }
		});
	}
}
