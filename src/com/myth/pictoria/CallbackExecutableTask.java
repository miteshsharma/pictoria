package com.myth.pictoria;

public class CallbackExecutableTask implements Runnable {

	private final Runnable task;
	private final ICallback callback;
	
	public CallbackExecutableTask(Runnable task, ICallback callback) {
	    this.task = task;
	    this.callback = callback;
	}
	
	@Override
	public void run() {
		this.task.run();
		this.callback.onComplete();
	}

}
