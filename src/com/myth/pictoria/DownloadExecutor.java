package com.myth.pictoria;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DownloadExecutor {

	private final static ExecutorService serverTask = Executors.newFixedThreadPool(2);

	public static void submit(Runnable task, ICallback callbackListener) {
		serverTask.submit(new CallbackExecutableTask(task, callbackListener));
	}
}