package com.myth.pictoria;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

public class WebDownloader implements Runnable {

	private RequestHandler requestManager;
	
	public WebDownloader(RequestHandler requestManager) {
		this.requestManager = requestManager;
	}
	
	@Override
	public void run() {
		try {
			this.serverResponse(this.requestManager.requestDetail.getImageUrl(), this.requestManager.requestDetail.getFilePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void serverResponse(String urlString, String filePath) throws IOException {
		int maxTries = ServerConfig.SERVER_RETRIES;
		InputStream input = null;
		OutputStream output = null;
		HttpURLConnection httpConn = null;
		while(maxTries > 0) {
			try {
				if (Thread.interrupted()) throw new InterruptedException();
				
				URL url = new URL(urlString);
				httpConn = (HttpURLConnection)url.openConnection();
				httpConn.setReadTimeout(10000);
				httpConn.setConnectTimeout(10000);
				httpConn.setRequestMethod("GET");
				httpConn.connect();
				
				if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
					input = new BufferedInputStream(url.openStream());
					requestManager.requestDetail.getAssetHandler().addAsset(input);
					input.close();
			        return;
				}
			} catch(Exception ex) {
				if (ex instanceof SocketTimeoutException) {
					// Max timeout limit exceeded.
					maxTries = 0;
				}
				if(ex.getClass().equals(FileNotFoundException.class)) {
					// File not found on server.
				}
				if (maxTries > 0) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					maxTries --;
				} else {
					ex.printStackTrace();
				}
			} finally {
				try {
	                if (output != null)
	                    output.close();
	                if (input != null)
	                    input.close();
	            } catch (IOException ignored) {
	            }

	            if (httpConn != null)
	            	httpConn.disconnect();
			}
		}
	}
}
