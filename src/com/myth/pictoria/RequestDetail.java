package com.myth.pictoria;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class RequestDetail {
	private String imageUrl;
	ImageView imageView;
	int defaultResourceId;
	int errorResourceId;
	ManagementType managementType;
	private IAssetHandler currentAssetHandler;
	int height;
	int width;

	public enum ManagementType {
		CACHE,
		DISK;
	}
	
	public RequestDetail(String imageUrl, ImageView imageView, int defaultResourceId, int errorResourceId, ManagementType managementType) {
		this.imageUrl = imageUrl;
		this.imageView = imageView;
		this.defaultResourceId = defaultResourceId;
		this.errorResourceId = errorResourceId;
		this.managementType = managementType;
	}
	
	public RequestDetail(String imageUrl, ImageView imageView, int defaultResourceId) {
		this.imageUrl = imageUrl;
		this.imageView = imageView;
		this.defaultResourceId = defaultResourceId;
		this.errorResourceId = defaultResourceId;
		this.managementType = ManagementType.CACHE;
	}
	
	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public String getImageUrl() {
		return this.imageUrl;
	}
	
	public Drawable getResource(int resourceId) {
		return Pictoria.getInstance().context.getResources().getDrawable(resourceId);
	}
	
	public Drawable getDefaultResource() {
		return this.getResource(defaultResourceId);
	}
	
	public Drawable getErrorResource() {
		if (errorResourceId == 0) {
			this.errorResourceId = this.defaultResourceId;
		}
		return this.getResource(errorResourceId);
	}
	
	public ManagementType getManagementType() {
		if (managementType == null) {
			return ManagementType.CACHE;
		}
		return managementType;
	}

	public IAssetHandler getAssetHandler() {
		if (ManagementType.DISK.equals(managementType)) {
			currentAssetHandler = new AssetFileHandler(this);
		} else {
			// We keep no cache item saved and release them once used.
			currentAssetHandler = new AssetCacheHandler(this);
		}
		return currentAssetHandler;
	}
	
	public String getFilePath() {
		String fileName = this.imageUrl.substring( this.imageUrl.lastIndexOf('/')+1, this.imageUrl.length() );
		if (this.imageUrl.contains("content:")) {
			String[] imageStr = this.imageUrl.split("/");
			fileName = imageStr[imageStr.length-2];
		}
		return Pictoria.getInstance().getAssetHandleManager().getFilePath() +"/" + fileName;
	}
}
