package com.myth.pictoria;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;

public class ContactDownloader implements Runnable {

	private RequestHandler requestManager;
	
	public ContactDownloader(RequestHandler requestManager) {
		this.requestManager = requestManager;
	}
	
	@Override
	public void run() {
		try {
			Uri imageUri = Uri.parse(this.requestManager.requestDetail.getImageUrl());
			Cursor cursor = Pictoria.getInstance().context.getContentResolver().query(imageUri,
			          new String[] {Contacts.Photo.PHOTO}, null, null, null);
		     if (cursor == null) {
		         return;
		     }
		     try {
		         if (cursor.moveToFirst()) {
		             byte[] data = cursor.getBlob(0);
		             if (data != null) {
		                 InputStream input = new ByteArrayInputStream(data);
		                 requestManager.requestDetail.getAssetHandler().addAsset(input);
		             }
		         }
		     } finally {
		         cursor.close();
		     }
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

}
