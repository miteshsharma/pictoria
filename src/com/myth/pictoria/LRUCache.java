package com.myth.pictoria;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class LRUCache<Key, Value> {

	private Map<Key, Value> cacheMap;
	private LinkedList<Key> cacheQueue;
	private static int DEFAULT_SIZE = 10;
	private int maxSize;
	
	public LRUCache() {
		this(DEFAULT_SIZE);
	}
	
	public LRUCache(int maxSize) {
		if (maxSize <= 0) {
			throw new IllegalArgumentException("Lru size can not be 0 or less than 0.");
		}
		this.maxSize = maxSize;
		this.cacheMap = new HashMap<Key, Value>();
		this.cacheQueue = new LinkedList<Key>();
	}
	
	public void put(Key key, Value value) {
		if (key == null) {
			throw new IllegalArgumentException("Lru cache key can not be null");
		}

		if (cacheMap.containsKey(key)) {
			this.cacheQueue.remove(key);
		}

		while(cacheQueue.size() >= this.maxSize) {
			Key oldKey = cacheQueue.pollLast();
			if (oldKey != null) {
				cacheMap.remove(oldKey);
			}
		}

		cacheQueue.add(0, key);
		cacheMap.put(key, value);
	}
	
	public Value get(Key key) {
		return cacheMap.get(key);
	}
	
	public void clear() {
		cacheMap.clear();
		cacheQueue.clear();
	}
	
}
