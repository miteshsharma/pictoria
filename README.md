# README #
This library is used to fetch images from server or contacts based on provided link. We can cache file which can be used multiple times during application time line. This library also provide functionality to download file and store it on mobile, so that we don't need to download it multiple times. Cached files needs to be download again when used on next start of application.

Developer can provide how many cached files we want based on memory need. We can show default image until we have download image or contact image ready to be replaced. If due to some reason, we are unable to download file, then we can show error resource id which will take place of default resouce in imageview.

### What is this repository for? ###

* Library used to download bitmap file from server or contacts and updating in Image view.
* Handle Default and error image if image is not downloaded properly.
* This is v1.0.0

### How do I get set up? ###

* Use this as a library. Take zip file and add it in library.

### Contribution guidelines ###

* Please comment your code properly.